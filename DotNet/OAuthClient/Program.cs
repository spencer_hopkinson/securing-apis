﻿using System.Net.Http.Json;
using System.Text.Json.Serialization;

const string Host = "http://localhost:5228";

string token = await CreateToken();

CacheClient cacheClient = new CacheClient();
cacheClient.SetCacheData<string>("AccessToken", token, TimeSpan.FromSeconds(50));

while (true)
{
    token = cacheClient.GetCachedData<string>("AccessToken");
    if (string.IsNullOrEmpty(token))
    {
        Console.WriteLine("Getting new token!");
        token = await CreateToken();
        Console.WriteLine("Caching new token!");
        cacheClient.SetCacheData<string>("AccessToken", token, TimeSpan.FromSeconds(50));
    }

    await UseToken(token);
    Thread.Sleep(10000);
}

async Task UseToken(string token)
{
    HttpClient httpClient = new HttpClient();

    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, $"{Host}/protected");

    request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

    HttpResponseMessage response = await httpClient.SendAsync(request);

    response.EnsureSuccessStatusCode();

    Console.WriteLine(response.StatusCode);
}

async Task<string> CreateToken()
{
    HttpClient httpClient = new HttpClient();

    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, $"{Host}/token/createtoken?grantType=client_credential");
    HttpResponseMessage response = await httpClient.SendAsync(request);

    response.EnsureSuccessStatusCode();

    Console.WriteLine(response.Content.ReadAsStringAsync().Result);

    Token token = await response.Content.ReadFromJsonAsync<Token>();

    return token.AccessToken;
}

class Token
{
    [JsonPropertyName("access_token")]
    public string AccessToken { get; set; }
}