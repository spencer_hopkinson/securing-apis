﻿using System.Web.Http;

namespace Secure.Api.Controllers
{
    [Authorize]
    public class BasicAuthController : ApiController
    {
        public IHttpActionResult Get()
        {
            string result = string.Format("BasicAuthSuccess - User Is:{0}", RequestContext.Principal.Identity.Name);
            return Ok<string>(result);
        }
    }
}