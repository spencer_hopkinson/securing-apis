﻿param($subject="localhost")

Clear-Host

<#
openssl req -x509 -nodes -newkey rsa:4096 -keyout "$($subject).key" -out "$($subject).pem" -days 365 -subj "/CN=$($subject)"

New-SelfSignedCertificate -certstorelocation cert:\CurrentUser\my `
    -Subject "Test User" -FriendlyName "Test client certificate" `
    -NotAfter (Get-Date).AddYears(1) -KeyUsageProperty Sign `
    -KeyUsage CertSign, CRLSign, DigitalSignature
#>

Remove-Item -path "*.crt"
Remove-Item -path "*.pfx"

Get-ChildItem Cert:\LocalMachine\my\ | Where-Object {$_.FriendlyName -eq 'root_ca_dev_shopkinson.com'} | Remove-Item

#Create root CA
New-SelfSignedCertificate -DnsName "root_ca_dev_shopkinson.com", "root_ca_dev_shopkinson.com" `
    -CertStoreLocation "cert:\LocalMachine\My" `
    -NotAfter (Get-Date).AddYears(1) `
    -FriendlyName "root_ca_dev_shopkinson.com" `
    -KeyUsageProperty All `
    -KeyUsage CertSign, CRLSign, DigitalSignature

$rootcert = Get-ChildItem Cert:\LocalMachine\my\ | Where-Object {$_.FriendlyName -eq 'root_ca_dev_shopkinson.com'}

if(!$rootcert)
{
    Write-Error "no root cert found"
    exit 1
}

$mypwd = ConvertTo-SecureString -String "1234" -Force -AsPlainText
Get-ChildItem -Path "cert:\localMachine\my\$($rootcert.Thumbprint)" | Export-PfxCertificate -FilePath "root_ca_dev_shopkinson.pfx" -Password $mypwd
Export-Certificate -Cert "cert:\localMachine\my\$($rootcert.Thumbprint)" -FilePath "root_ca_dev_shopkinson.crt"

#Create child cert from root

Write-Host "Root cert: $($rootcert.Subject)"

function CreateClientCert($name, $certStore)
{
    Write-Host "Removing $($name)" -ForegroundColor Yellow
    Get-ChildItem -Path $certStore | Where-Object {$_.FriendlyName -eq $name} | Remove-Item

    New-SelfSignedCertificate -certstorelocation $certStore `
        -dnsname $name `
        -Signer $rootcert `
        -NotAfter (Get-Date).AddYears(1) `
        -FriendlyName $name
}

<#
Get-ChildItem Cert:\LocalMachine\my\ | Where-Object {$_.FriendlyName -eq 'child_a_dev_shopkinson.com'} | Remove-Item

New-SelfSignedCertificate -certstorelocation "cert:\localmachine\My" `
    -dnsname "child_a_dev_shopkinson.com" `
    -Signer $rootcert `
    -NotAfter (Get-Date).AddYears(1) `
    -FriendlyName "child_a_dev_shopkinson.com"`
#>

# invalid client cert at machine store
$invalidCertName = "invalid_client"
CreateClientCert -name $invalidCertName -certStore "cert:\localmachine\My"
$cert = Get-ChildItem Cert:\LocalMachine\my\ | Where-Object {$_.FriendlyName -eq $invalidCertName}
$mypwd = ConvertTo-SecureString -String "1234" -Force -AsPlainText
Get-ChildItem -Path "cert:\localMachine\my\$($cert.Thumbprint)" | Export-PfxCertificate -FilePath "$($invalidCertName).pfx" -Password $mypwd
Export-Certificate -Cert "cert:\localMachine\my\$($cert.Thumbprint)" -FilePath "$($invalidCertName).crt"

# valid client cert at personal store
$validCertName = "valid_client"
CreateClientCert -name $validCertName -certStore "cert:\currentuser\My"
$cert = Get-ChildItem Cert:\currentuser\my\ | Where-Object {$_.FriendlyName -eq $validCertName}
$mypwd = ConvertTo-SecureString -String "1234" -Force -AsPlainText
Get-ChildItem -Path "cert:\currentuser\my\$($cert.Thumbprint)" | Export-PfxCertificate -FilePath "$($validCertName).pfx" -Password $mypwd
Export-Certificate -Cert "cert:\currentuser\my\$($cert.Thumbprint)" -FilePath "$($validCertName).crt"