REM Run this in a VS cmd prompt

makecert.exe -r -n "CN=My Personal CA" -pe -sv MyPersonalCA.pvk -a sha1 -len 2048 -b 01/01/2018 -e 01/01/2020 -cy authority MyPersonalCA.cer

makecert.exe -iv MyPersonalCA.pvk -ic MyPersonalCA.cer -n "CN=John Doe" -pe -sv JohnDoe.pvk -a sha256 -sky exchange JohnDoe.cer -eku 1.3.6.1.5.5.7.3.2

pvk2pfx.exe -pvk JohnDoe.pvk -spc JohnDoe.cer -pfx JohnDoe.pfx -po password