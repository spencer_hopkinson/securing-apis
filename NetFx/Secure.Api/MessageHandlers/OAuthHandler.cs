﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http.Headers;
using System.Net;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;
using System.Web;

namespace Secure.Api.MessageHandlers
{
    public class OAuthHandler : DelegatingHandler
    {
        private const string Scheme = "Bearer";
        private const string Key = "RSBSMzObvyh2Kjw9sFswKSKLe4IXIv6v";

        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpRequestHeaders headers = request.Headers;
            
            if (headers.Authorization != null && Scheme.Equals(headers.Authorization.Scheme))
            {
                JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

                TokenValidationParameters validationParameters = new TokenValidationParameters
                {
                    ValidAudiences = new string[] {"Audience" },    // Who or what the token is for
                    ValidIssuers = new string[] { "Issuer"},        // Who created and signed the token
                    ValidateAudience = true,
                    ValidateIssuer = true,
                    ValidateLifetime = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key))
                };

                try
                {
                    SecurityToken token;

                    ClaimsPrincipal claimsPrincipal = tokenHandler.ValidateToken(headers.Authorization.Parameter, validationParameters, out token);

                    Thread.CurrentPrincipal = claimsPrincipal;
                    HttpContext.Current.User = claimsPrincipal;
                }
                catch(SecurityTokenExpiredException tokenExpiredException)
                {
                    //return request.CreateResponse(HttpStatusCode.Unauthorized);

                    // Log the auth failure, then let code continue to the underlying controller,
                    // if it allows non authorised requests it will respond.
                    // If it does not, then it will return a 401 and trigger logic below to return a 401
                }
            }

            HttpResponseMessage response = await base.SendAsync(request, cancellationToken);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                response.Headers.WwwAuthenticate.Add(new AuthenticationHeaderValue(Scheme));
            }

            return response;
        }
    }
}