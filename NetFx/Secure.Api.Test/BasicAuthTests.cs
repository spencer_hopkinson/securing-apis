﻿using NUnit.Framework;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Secure.Api.Test
{    
    public class BasicAuthTests
    {        
        private const string AUTH_SCHEME = "Basic";
        
        public BasicAuthTests()
        {
            ServicePointManager.ServerCertificateValidationCallback =
                (object sender, X509Certificate cer, X509Chain chain, SslPolicyErrors error) =>
                {
                    return true; // This is done because we are using self-signed cert in server side - Not production strength
                };
        }

        [Test]
        public void No_Auth_Header_Returns_401()
        {
            HttpResponseMessage response = null;

            using(var client = new HttpClient())
            {
                client.BaseAddress = Utility.Host;
                response = client.GetAsync("api/basicauth").Result;
            }
            
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Test]
        public void With_Auth_Header_Returns_200()
        {
            string credentials = "apiuser:apipassword";
            var bytes = Encoding.ASCII.GetBytes(credentials);
            HttpResponseMessage response = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = Utility.Host;
                client.DefaultRequestHeaders.Authorization = Utility.CreateAuthenticationHeader(AUTH_SCHEME, Convert.ToBase64String(bytes));
                response = client.GetAsync("api/basicauth").Result;
            }
            
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void With_Auth_Header_And_Incorrect_Credentials_Returns_401()
        {
            string credentials = "apiuser:xxxx";
            var bytes = Encoding.ASCII.GetBytes(credentials);
            HttpResponseMessage response = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = Utility.Host;
                client.DefaultRequestHeaders.Authorization = Utility.CreateAuthenticationHeader(AUTH_SCHEME, Convert.ToBase64String(bytes));
                response = client.GetAsync("api/basicauth").Result;
            }
                        
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }        
    }
}