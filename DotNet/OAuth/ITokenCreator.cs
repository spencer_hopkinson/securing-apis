namespace OAuthApi;

public interface ITokenCreator
{
    string CreateToken(UserModel user);
}