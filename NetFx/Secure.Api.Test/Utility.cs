﻿using System;
using System.Net.Http.Headers;

namespace Secure.Api.Test
{
    public static class Utility
    {
        public static readonly Uri Host = new Uri("https://localhost/");
        public static readonly Uri SecureHost = new Uri("https://localhost/");

        public static AuthenticationHeaderValue CreateAuthenticationHeader(string scheme, string parameter)
        {
            return new AuthenticationHeaderValue(scheme, parameter);
        }
    }
}
