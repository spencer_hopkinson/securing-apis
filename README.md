# README #

## What is this repository for? ###

Demonstration of different ways of securing RESTful web services.

## NETFX (.Net Framework 4.8)

Currently demonstrates:

* Basic Authentication (username and password base 64 encoded in auth header)
* Client certificate authentication
* OAuth bearer token (JWT)
* SAML (IN PROGRESS)
* DIGEST (IN PROGRESS)

### How do I get set up? ###

* Create a web site in IIS, v4 app pool, bindings: localhost:8080 and https (assign a self signed cert)
* In the local computer context, add the certificate "MyPersonalCA.cer" to the Trusted Root CA store
* Run the tests!


## DotNet/ClientCerts (.net8)

This is an example of securing an api with client certs. The api is hard coded to allow a certificate with a subject of "valid_client". Any other cert will be denied.

### How To Use

* Create the root and client certs via the ```create-certs``` powershell script
* Three certs will be generated, one root CA and two client certs
* The root and client cert "invalid_client" will be in the machine level personal store
* A "valid_client" cert will be in your local user personal store
* The root certificate **must** be in the trusted root cert authority, it is put into the intermediate cert authorities by default. Simply move it to the root store in your personal cert store (not machine)
* Run the api with ```dotnet run```
* Test the api with ```ApiCallWithCert.ps1```
* * This will access the api with a valid certificate and the api will return OK
* * This will access the api with the invalid certificate and the api will return 403 (Forbidden)
* You can also test with the browser, go to https://localhost:7209/weatherforecast
* * The browser should prompt on what client certificate to use
* * The valid certificate from your personal store should be listed
* * Select the valid certificate, and you'll be authenticated and access the api ok

That's it!

## DotNet/OAuth (.net8)

### OAuth / Bearer Token Demo

This is a sample of using OAuth with bearer tokens for securing API's.

Project is C# .net8

## DotNet/OAuthClient

A sample console app that will create oauth tokens from the OAuth API. This also uses a Redis store to cache the tokens to handle lifetime.

Start Redis container via:

```docker run -d --rm --name redis -p 6379:6379 redis:6.2.14-alpine```