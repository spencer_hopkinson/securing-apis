﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Secure.Api.MessageHandlers
{
    public class BasicAuthHandler : DelegatingHandler
    {
        private const string Scheme = "Basic";

        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            try
            {
                var headers = request.Headers;
                if (headers.Authorization != null && Scheme.Equals(headers.Authorization.Scheme))
                {
                    Encoding encoding = Encoding.GetEncoding("iso-8859-1");

                    string credentials = encoding.GetString(Convert.FromBase64String(
                                                                           headers.Authorization.Parameter));
                    string[] parts = credentials.Split(':');
                    string userId = parts[0].Trim();
                    string password = parts[1].Trim();

                    var isValidated = userId.Equals("apiuser", StringComparison.InvariantCultureIgnoreCase) && 
                        password.Equals("apipassword", StringComparison.InvariantCultureIgnoreCase);

                    if (isValidated)
                    {
                        var claims = new List<Claim>
                        {
                                new Claim(ClaimTypes.Name, userId),
                                new Claim(ClaimTypes.AuthenticationMethod, AuthenticationMethods.Password)
                        };

                        var principal = new ClaimsPrincipal(
                                                        new[] { new ClaimsIdentity(claims, Scheme) });

                        Thread.CurrentPrincipal = principal;                        
                        
                        //Use the following to avoid the HttpContext reference and allow unit testing:
                        //(request.Properties["MS_HttpContext"] as HttpContextBase).User = principal;   
                        HttpContext.Current.User = principal;
                    }
                }

                var response = await base.SendAsync(request, cancellationToken);

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    response.Headers.WwwAuthenticate.Add(
                              new AuthenticationHeaderValue(Scheme));
                }

                return response;
            }
            catch (Exception)
            {
                var response = request.CreateResponse(HttpStatusCode.Unauthorized);
                response.Headers.WwwAuthenticate.Add(new AuthenticationHeaderValue(Scheme));

                return response;
            }
        }       
    }
}