﻿using System.Web.Http;

namespace Secure.Api.Controllers
{
    [Authorize]
    public class ClientCertController : ApiController
    {
        public IHttpActionResult Get()
        {
            string result = string.Format("ClientCertSuccess - User Is:{0}", RequestContext.Principal.Identity.Name);
            return Ok<string>(result);
        }
    }
}
