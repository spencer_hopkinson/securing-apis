using System;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Authentication.Certificate;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.Server.Kestrel.Https;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.Configure<KestrelServerOptions>(options =>
{
    options.ConfigureHttpsDefaults(options => 
    {
        options.ClientCertificateMode = ClientCertificateMode.RequireCertificate;
        options.AllowAnyClientCertificate();
    });
});

builder.Logging.AddConsole();
builder.Services.AddControllers();

Console.WriteLine("Starting...");

builder.Services.AddAuthentication(CertificateAuthenticationDefaults.AuthenticationScheme)
    .AddCertificate(options =>
    {                
        Console.WriteLine("In AddCertificate...");
        options.AllowedCertificateTypes = CertificateTypes.All;
        options.RevocationMode = X509RevocationMode.NoCheck;

        options.Events = new CertificateAuthenticationEvents
        {
            OnAuthenticationFailed = context =>
            {
                Console.WriteLine("Certificate authentication failed");
                context.Fail("Invalid certificate");
                return Task.CompletedTask;
            },
            OnCertificateValidated = context =>
            {
                Console.WriteLine($"Certification subject: {context.ClientCertificate.Subject} Thumb: {context.ClientCertificate.Thumbprint}");                
                if(context.ClientCertificate.Subject=="CN=valid_client")
                {
                    Console.WriteLine("Certificate is valid, authorised");
                    context.Success();
                }
                else
                {
                    Console.WriteLine("Certificate is NOT valid, not authorised");
                    context.Fail("Invalid certificate!");
                }
                return Task.CompletedTask;
            }
        };

    });

var app = builder.Build();

app.UseAuthentication();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();