﻿using NUnit.Framework;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace Secure.Api.Test
{
    public class ClientCertAuthTests
    {
        public ClientCertAuthTests()
        {
            ServicePointManager.ServerCertificateValidationCallback =
                (object sender, X509Certificate cer, X509Chain chain, SslPolicyErrors error) =>
                {
                    return true; // This is done because we are using self-signed cert in server side - Not production strength
                };
        }

        [Test]
        public void No_Cert_Returns_401()
        {
            HttpResponseMessage response = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = Utility.Host;
                response = client.GetAsync("api/ClientCertAuth").Result;
            }
            
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        
        [Test]
        public void Valid_Cert_Returns_200()
        {            

            /*
            YOU MUST ENABLE ACCEPT CLIENT CERTIFICATES IN IIS!!
            SITE->SSL SETTINGS->CLIENT CERTIFICATES: ACCEPT
            */

            var handler = new WebRequestHandler();
            //var thumbprint = "df 24 74 7f 06 0a 16 47 9d 71 f6 93 16 11 bd 96 8a ac 8d 5a";

            //Set current directory to executing path so relative paths below work
            System.IO.Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);

            //handler.ClientCertificates.Add(GetCertificate(StoreLocation.CurrentUser, X509FindType.FindByThumbprint, thumbprint));
            handler.ClientCertificates.Add(GetCertificate(@"..\..\..\Certs\johndoe.pfx"));

            HttpResponseMessage response = null;

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = Utility.SecureHost;
                response = client.GetAsync("api/ClientCertAuth").Result;
            }

            Console.WriteLine(response.Content.ReadAsStringAsync().Result);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private static X509Certificate2 GetCertificate(string certFile)
        {
            return new X509Certificate2(certFile, "password");            
        }

        private static X509Certificate2 GetCertificate(StoreLocation storeLocation, X509FindType findType, string findValue)
        {
            X509Store certStore = new X509Store(storeLocation);
            certStore.Open(OpenFlags.ReadOnly);

            var certificates = certStore.Certificates.Find(findType, findValue, false);

            if (certificates.Count > 1)
            {
                throw new InvalidOperationException("Found multiple certificates");
            }

            return certificates[0];
        }
    }
}