﻿Clear-Host

function InvokeApi-WithCert($certFile)
{
    $cert = Get-PfxCertificate -FilePath $certFile

    if(!$cert)
    {
        throw "cert is null!"
    }

    Write-Host "Subject=$($cert.Subject) FriendlyName=$($cert.FriendlyName)"

    if($cert.HasPrivateKey -eq $false)
    {
        Write-Host "No private key"
    }

    Invoke-WebRequest -Uri "https://localhost:7209/weatherforecast" -Certificate $cert -UseBasicParsing
}

Write-Host ([System.Net.ServicePointManager]::SecurityProtocol)
#[System.Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
#Write-Host ([System.Net.ServicePointManager]::SecurityProtocol)
#[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}

InvokeApi-WithCert -certFile "./valid_client.pfx"
InvokeApi-WithCert -certFile "./invalid_client.pfx"