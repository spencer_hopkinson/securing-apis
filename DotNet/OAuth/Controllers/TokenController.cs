namespace OAuthApi.Controllers;

using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("[controller]")]
public class TokenController : ControllerBase
{
    private readonly ITokenCreator _tokenCreator;

    public TokenController(ITokenCreator tokenCreator)
    {
        _tokenCreator = tokenCreator;
    }

    [HttpPost]
    [Route("CreateToken")]
    public async Task<IActionResult> CreateToken(string grantType)
    {
        return Ok(new
        {
            access_token = _tokenCreator.CreateToken(new UserModel
            {
                Firstname = "spencer",
                Email = "spencer.hopkinson@adp.com"
            })
        });
    }
}