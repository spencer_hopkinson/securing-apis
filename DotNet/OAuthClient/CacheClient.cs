using System.Text.Json;
using StackExchange.Redis;

class CacheClient
{
    private readonly IDatabase _db;

    public CacheClient()
    {
        IConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost");
        _db = redis.GetDatabase();
    }

    internal void SetCacheData<T>(string key, T value)
    {
        _db.StringSet(key, JsonSerializer.Serialize(value));
    }

    internal void SetCacheData<T>(string key, T value, TimeSpan duration)
    {
        _db.StringSet(key, JsonSerializer.Serialize(value), duration);
    }

    internal T GetCachedData<T>(string key)
    {
        string json = _db.StringGet(key);

        if (string.IsNullOrEmpty(json))
        {
            return default(T);
        }

        return JsonSerializer.Deserialize<T>(json);
    }
}