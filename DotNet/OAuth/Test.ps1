clear-host

$response = Invoke-WebRequest http://localhost:5228/protected -Method Get

$response = Invoke-WebRequest http://localhost:5228/token/createtoken?granttype=client_credential -Method Post

$token = (ConvertFrom-Json $response.Content).access_token
$token;

(ConvertTo-Json @{Authorization="Bearer $token"})

Invoke-WebRequest http://localhost:5228/protected -Method Get -Headers @{Authorization="Bearer $token"}