namespace OAuthApi;

public class UserModel
{
    public string Firstname { get; set; }
    public string Lastname { get; set; }
    public string Email { get; set; }
    public int RoleId { get; set; }

    public string Fullname()
    {
        return $"{Firstname} {Lastname}";
    }
}