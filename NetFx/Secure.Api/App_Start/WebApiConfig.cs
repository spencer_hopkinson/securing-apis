﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace Secure.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {            
            // Web API routes
            config.MapHttpAttributeRoutes();
            
            config.Routes.MapHttpRoute(
                name: "BasicAuth",
                routeTemplate: "api/BasicAuth/{id}",
                defaults: new { id = RouteParameter.Optional,controller="BasicAuth" },
                constraints:null,
                handler:CreateBasicAuthHandler(config)
            );

            config.Routes.MapHttpRoute(
                name: "ClientCertAuth",
                routeTemplate: "api/ClientCertAuth/{id}",
                defaults: new { id = RouteParameter.Optional, controller = "ClientCert" },
                constraints: null,
                handler: CreateClientCertAuthHandler(config)
            );

            config.Routes.MapHttpRoute(
                name: "OAuth",
                routeTemplate: "api/Oauth/{id}",
                defaults: new { id = RouteParameter.Optional, controller = "OAuth" },
                constraints: null,
                handler: CreateOAuthHandler(config)
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }                
            );
        }

        private static HttpMessageHandler CreateBasicAuthHandler(HttpConfiguration config)
        {
            DelegatingHandler[] handler = new DelegatingHandler[]
            {
                new MessageHandlers.BasicAuthHandler()
            };

            return HttpClientFactory.CreatePipeline(new HttpControllerDispatcher(config), handler);
        }

        private static HttpMessageHandler CreateClientCertAuthHandler(HttpConfiguration config)
        {
            DelegatingHandler[] handler = new DelegatingHandler[]
            {
                new MessageHandlers.ClientCertAuthHandler()
            };

            return HttpClientFactory.CreatePipeline(new HttpControllerDispatcher(config), handler);
        }

        private static HttpMessageHandler CreateOAuthHandler(HttpConfiguration config)
        {
            DelegatingHandler[] handler = new DelegatingHandler[]
            {
                new MessageHandlers.OAuthHandler()
            };

            return HttpClientFactory.CreatePipeline(new HttpControllerDispatcher(config), handler);
        }
    }
}
