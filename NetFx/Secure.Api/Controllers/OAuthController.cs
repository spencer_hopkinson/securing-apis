﻿using System.Linq;
using System.Security.Claims;
using System.Web.Http;

namespace Secure.Api.Controllers
{
    [Authorize]
    public class OAuthController : ApiController
    {
        public IHttpActionResult Get()
        {
            string name = (User as ClaimsPrincipal).Claims.SingleOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;
            return Ok($"You are authorised {name} !");
        }
    }
}